""" AniCReq: hash files and creq them. Also add them to your account if you really want. """
import argparse
import threading

from helpers.ezhash import EZHash


DESCRIPTION = 'Process some video files for AniDB creqing.'


def add_args():
    """
    Add the arguments for operation to the program.
    :return: program arguments, parsed.
    """

    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('path_to_file', type=str, help='Path to the file or folder to analyze')

    parser.add_argument('--crc32', help='Run a CRC32 check on the given file.', action='store_true')
    parser.add_argument('--md4', help='Run a MD4 check on the given file.', action='store_true')
    parser.add_argument('--md5', help='Run a MD5 check on the given file.', action='store_true')
    parser.add_argument('--ed2k', help='Run a ED2K check on the given file.', action='store_true')
    parser.add_argument('--sha1', help='Run a SHA1 check on the given file.', action='store_true')

    return parser.parse_args()


def process_file(file, args):
    """
    Process a single file with whatever needs to be done.
    :param file: file to process
    :param args: program arguments like the flags
    """
    file_details = dict()
    if args.crc32:
        file_details['crc32'] = EZHash.crc32(file)
    if args.md4:
        file_details['md4'] = EZHash.md4(file)
    if args.sha1:
        file_details['sha1'] = EZHash.sha1(file)
    if args.ed2k:
        file_details['ed2k'] = EZHash.ed2k(file)
    if args.md5:
        file_details['md5'] = EZHash.md5(file)
    return file_details


def main():
    """ Main """
    args = add_args()
    details = process_file(args.path_to_file, args)
    for key, value in details.items():
        print('{}: {}'.format(key, value))


if __name__ == "__main__":
    main()
