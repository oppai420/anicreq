"""
Collection of self-implemented hashing methods
"""
import hashlib
import zlib


class HashBase:
    """ Base hash class.
    """

    def __init__(self):
        self._digest = 0

    def copy(self):
        """
        Create a copy of this object.
        :return: a copy of this base
        """
        __class__ # todo: test if I *really* need this
        copy = super().__new__(self.__class__)
        copy._digest = self._digest
        return copy

    def digest(self):
        """ :return: the digest of the object. """
        return self._digest

    def hexdigest(self):
        """ :return: the digest of the object in hex format. """
        return '{:08x}'.format(self._digest)


class Crc32(HashBase):
    """
    CRC32 implementation based on:
    https://stackoverflow.com/questions/1742866/compute-crc-of-file-in-python/5061842#5061842
    """
    name = 'crc32'
    digest_size = 4
    block_size = 1

    def __init__(self, arg=b''):
        super().__init__()
        self.update(arg)

    def update(self, arg):
        """
        Updates the digest with the given data
        :param arg: data to be crc32'd by zlib
        """
        self._digest = zlib.crc32(arg, self._digest) & 0xffffffff


class Ed2k(HashBase):
    """
    eDonkey2000 hash method, based on: https://www.radicand.org/?p=60
    """

    BLOCK_SIZE = 9728000
    __part_list = []
    md4 = hashlib.new('md4').copy

    def __md4_digest(self, data):
        """
        MD4 sum data
        :param data: data to be summed
        :return: an md4 hashlib object
        """
        md4_obj = self.md4()
        md4_obj.update(data)
        return md4_obj.digest()

    def hexdigest(self):
        """
        Get the hex digest. As it's digest is a bytes object, all that has to be done is call .hex()
        :return: hex digest as a string
        """
        return self._digest.hex()

    def update(self, arg):
        """
        Update the hex digest of the object
        :param arg: data to update the digest with
        """
        self.__part_list.append(self.__md4_digest(arg))
        if len(self.__part_list) == 1:
            self._digest = self.__part_list[0]
        else:
            self._digest = self.__md4_digest(b''.join(self.__part_list))
