""" Helpers for easy hashing. """
import hashlib

from tqdm import tqdm

from helpers import hashes

# Add our implementation of CRC32 for hashlib
hashlib.crc32 = hashes.Crc32
hashlib.algorithms_available.add('crc32')
# Add our implementation of ED2K for hashlib
hashlib.ed2k = hashes.Ed2k
hashlib.algorithms_available.add('ed2k')


class EZHash:
    """
    Holds all of the hashing functions for AniCReq
    """

    @staticmethod
    def hash(file, hash_method, default_block_size=1024):
        """
        Base function for hashing files.
        :param file: file to hash
        :param hash_method: the method of which to hash it with
        :param default_block_size: size of the chunks that should be taken when hashing
               this does not affect ed2k due to the standardized chunk size
        :return: the hex formatted hash digest of the file
        """
        with open(file, 'rb') as opened_file:
            with tqdm(total=len(opened_file.read())) as pbar:
                opened_file.seek(0)
                while True:
                    data_part = opened_file.read(
                        hash_method.BLOCK_SIZE if hasattr(hash_method, 'BLOCK_SIZE')
                        else default_block_size
                    )
                    if data_part:
                        hash_method.update(data_part)
                        pbar.update(len(data_part))
                    else:
                        break
            return hash_method.hexdigest()

    @staticmethod
    def crc32(file):
        """
        Hash a file with CRC32
        :param file: file to hash
        :return: hex formatted hash digest
        """
        return EZHash.hash(file, hashlib.crc32())

    @staticmethod
    def sha1(file):
        """
        Hash a file with SHA1
        :param file: file to hash
        :return: hex formatted hash digest
        """
        return EZHash.hash(file, hashlib.sha1())

    @staticmethod
    def ed2k(file):
        """
        Hash a file with eD2k.
        :param file: file to hash
        :return: hex formatted hash digest
        """
        return EZHash.hash(file, hashlib.ed2k())

    @staticmethod
    def tiger(file):
        """
        Hash a file with tiger.
        :param file: file to hash
        :return: hex formatted hash digest
        """
        # todo: Implement Tiger
        raise NotImplementedError()

    @staticmethod
    def md4(file):
        """
        Hash a file with MD4.
        :param file: file to hash
        :return: hex formatted hash digest
        """
        return EZHash.hash(file, hashlib.new('md4'))

    @staticmethod
    def md5(file):
        """
        Hash a file with MD5.
        :param file: file to hash
        :return: hex formatted hex digest
        """
        return EZHash.hash(file, hashlib.md5())

    @staticmethod
    def tth(file):
        """
        Hash a file with TTH.
        :param file: file to hash
        :return: hex formatted hash digest
        """
        # todo: Implement TTH
        raise NotImplementedError()
