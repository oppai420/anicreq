import unittest

from helpers.ezhash import EZHash


class TestHashMethods(unittest.TestCase):

    __TEST_FILE = "../testfile"

    def test_ed2k(self):
        self.assertEqual(
            EZHash.ed2k(self.__TEST_FILE),
            'a8d0b7745acb24eb36ced15a54bf2997'
        )

    def test_md4(self):
        self.assertEqual(
            EZHash.md4(self.__TEST_FILE),
            'a8d0b7745acb24eb36ced15a54bf2997'
        )

    def test_crc32(self):
        self.assertEqual(
            EZHash.crc32(self.__TEST_FILE),
            '0265eea1'
        )

    def test_sha1(self):
        self.assertEqual(
            EZHash.sha1(self.__TEST_FILE),
            '768af928467466e49ce04cfb2803ebc8d4014264'
        )

    def test_md5(self):
        self.assertEqual(
            EZHash.md5(self.__TEST_FILE),
            '0feca8ed1885628655175bf192303f9b'
        )

    def test_tiger(self):
        self.assertEqual(
            EZHash.tiger(self.__TEST_FILE),
            'b75d8a0af40b6638ac4ac311b630035b'
        )

    def test_tth(self):
        self.assertEqual(
            EZHash.tth(self.__TEST_FILE),
            'nsflxhl2i6tvv3spdvt5qnogyr4fzyvav5aykga'
        )
